extends TextEdit

#TODO:
#Integrointi ajastinsovellukseen
#niin että teksti taskibaarin päällä muuttuisi ja vaikka välkkyisi
#Save / load

const TASKBAR_HEIGHT = 50
const default_screen_size = Vector2(400, 360)
var pinned_to_taskbar = false
var screen_size = OS.get_screen_size(OS.get_current_screen())



func _ready():
    #right_align_window()
    add_keyword_color("TODO", Color(0.3,0.3,0.9,1))
    add_keyword_color("OK", Color(0.3,0.9,0.3,1))
    add_color_region("*","*",Color(0.9,0.3,0.3,1))
    add_color_region("(",")",Color(0.3,0.3,0.3,1))
    add_color_region("\"" , "\"",Color(0.9,0.7,0.6,1))
    
    
    

func _input(event):
    if event is InputEventKey:
        if !event.echo and event.pressed:
            if event.control:
                if event.shift:
                    if event.scancode == KEY_S:
                        $SaveFileDialog.popup()
                else:
                    if event.scancode == KEY_S:
                        $SaveFileDialog.popup()
            else:
                match event.scancode:
                    KEY_F1:
                        append_manual()
                    KEY_F8: #must be F when debugging (supposed to be F8)
                        if !OS.window_fullscreen:
                            toggle_pin_to_taskbar()
                    KEY_F9:
                        breakpoint_gutter = !breakpoint_gutter
                        show_line_numbers = breakpoint_gutter
                    KEY_F10:
                        if !OS.window_fullscreen:
                            OS.window_borderless = !OS.window_borderless
                    KEY_F11:
                        OS.set_window_fullscreen(!OS.window_fullscreen)
                    KEY_F12:
                        if !OS.window_fullscreen:
                            OS.set_window_always_on_top(!OS.is_window_always_on_top())
    
func append_manual():
    $PopUpManual.popup()

func toggle_pin_to_taskbar():
    #toggles between taskbar and right aligned
    screen_size = OS.get_screen_size(OS.get_current_screen())
    if not pinned_to_taskbar:
        pinned_to_taskbar = true
        OS.window_size = Vector2(600, TASKBAR_HEIGHT)
        OS.set_window_position(Vector2(
            screen_size.x / 2 - OS.window_size.x / 2,
            screen_size.y - OS.window_size.y * 2 + 10 #just over taskbar
        ))
        OS.set_window_always_on_top(true)
        OS.window_borderless = true
        show_line_numbers = false
    else:
        pinned_to_taskbar = false
        OS.set_window_always_on_top(false)
        show_line_numbers = true
        breakpoint_gutter = true
        right_align_window()

func right_align_window():
    OS.window_size = default_screen_size
    OS.set_window_position(Vector2(
        screen_size.x - default_screen_size.x,
        screen_size.y / 2 - OS.window_size.y / 2
    ))



func _on_SaveFileDialog_file_selected(path):
    var file = File.new()
    file.open(path,2)
    file.store_string(self.text)
    file.close()


func _on_OpenFileDialog_file_selected(path):
    pass # Replace with function body.
